package test;

import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.net.MalformedURLException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RedditSearchTest {
    private WebDriver driver;
    final By SKIP_BUTTON_READY_LOCATOR = By.id("skip_text");
    final By GOTIT_BUTTON_READY_LOCATOR = By.id("button1");
    final By SEARCH_VIEW_READY_LOCATOR = By.id("search_view");
    final By SEARCH_TEXT_READY_LOCATOR = By.id("search_src_text");
    final By COMMUNITY_RESULT_READY_LOCATOR = By.id("community_result_list");
    final By RELATIVE_LAYOUT_LOCATOR = By.className("android.widget.RelativeLayout");
    final By TEXT_VIEW_LOCATOR = By.className("android.widget.TextView");
    final By LINK_TITLE_READY_LOCATOR = By.id("link_title");

    @Before
    public void setup() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(CapabilityType.BROWSER_NAME, System.getProperty("BROWSER_NAME", "Android"));
        capabilities.setCapability(CapabilityType.VERSION, System.getProperty("VERSION", "8.1.0"));
        capabilities.setCapability("platformName", System.getProperty("PLATFORM_NAME", "Android"));
        capabilities.setCapability("deviceName", System.getProperty("DEVICE", "emulator-5554"));
        capabilities.setCapability("appPackage", System.getProperty("APP_PACKAGE", "com.reddit.frontpage"));
        capabilities.setCapability("appActivity", System.getProperty("APP_ACTIVITY", "com.reddit.frontpage.StartActivity"));
        capabilities.setCapability("app", RedditSearchTest.class.getResource("reddit.apk") );
        try{
            driver = new RemoteWebDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
        } catch (MalformedURLException ignore){
            Assert.assertTrue("The test failed due to wrong URL or no Appium server available", false);
        }
    }

    @Test
    public void checkForNintendoPost() {
        final WebDriverWait wait = new WebDriverWait(driver, 10);

        try {
            //Login screen
            ExpectedCondition<WebElement> skipButtonReady = ExpectedConditions.presenceOfElementLocated(SKIP_BUTTON_READY_LOCATOR);
            wait.until(skipButtonReady).click();

            //Terms popup
            ExpectedCondition<WebElement> gotItButtonReady = ExpectedConditions.presenceOfElementLocated(GOTIT_BUTTON_READY_LOCATOR);
            wait.until(gotItButtonReady).click();

            //Search input
            ExpectedCondition<WebElement> searchViewReady = ExpectedConditions.presenceOfElementLocated(SEARCH_VIEW_READY_LOCATOR);
            wait.until(searchViewReady).click();

            ExpectedCondition<WebElement> searchInputReady = ExpectedConditions.presenceOfElementLocated(SEARCH_TEXT_READY_LOCATOR);
            wait.until(searchInputReady).sendKeys("gaming");


            //Search dropdown for "r/gaming"
            ExpectedCondition<WebElement> dropdownReady = ExpectedConditions.presenceOfElementLocated(COMMUNITY_RESULT_READY_LOCATOR);
            wait.until(dropdownReady);
            List<WebElement> dropdownItems = driver.findElement(COMMUNITY_RESULT_READY_LOCATOR).findElements(RELATIVE_LAYOUT_LOCATOR);
            WebElement targetDropdown = null;
            for (WebElement element : dropdownItems) {
                if (element.findElements(TEXT_VIEW_LOCATOR).get(0).getText().equals("r/gaming"))
                    targetDropdown = element;
            }
            Assert.assertTrue("Couldn't find r/gaming!", targetDropdown != null);

            //Access subreddit
            targetDropdown.click();

            //Search for "nintendo" in first post's title
            ExpectedCondition<WebElement> subRedditPostsReady = ExpectedConditions.presenceOfElementLocated(LINK_TITLE_READY_LOCATOR);
            wait.until(subRedditPostsReady);
            List<WebElement> subRedditPosts = driver.findElements(LINK_TITLE_READY_LOCATOR);
            Assert.assertTrue("First post's title does not contain 'nintendo' substring", subRedditPosts.get(0).getText().toLowerCase().contains("nintendo"));
        } catch(final TimeoutException ignore){
            Assert.assertTrue("The test failed due to the app taking too long to render", false);
        } catch(final NoSuchElementException ignore){
            Assert.assertTrue("The test failed due an element being missing", false);
        }
    }

    @After
    public void tearDown() {
        this.driver.quit();
    }
}