# Touch Surgery - QA Assignment Solution

This project is the answer to Touch Surgery's QA assignment. 

The first part (Android QA Assignment) consisted in performing an automated test in the Reddit's Android app using the Appium and the Selenium framework. The goal was to search in subreddit's "r/gaming" if the first post's title contained the word "nintendo".

The second part (General QA Assignment) consisted of review, critique and strategy-building of a use case of quality assurance.

## Files

The Android QA Assignment's solution is built as a jUnit automated test, using Maven as the dependency manager. It simply consists of the contents of /src and /pom.xml on the root folder of this project.

The General QA Assignment's solution is written in /assignment_solution.md on the root folder of this project.

### Prerequisites

This project requires the following libraries and applications to run:

- Java SDK 1.8
- Maven 3.5.0
- Android device or emulator
- Appium 1.6.1


## Running the tests

To start the test, you should run maven's test command in the root folder:

```
mvn test
```

The configuration of the test environment is in the /pom.xml file under the <systemPropertyVariables> tag


## Author

**Gustavo Orlandini Fritsch**
