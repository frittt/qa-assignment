# Touch Surgery - General QA Assignment Solution

---

### 1. Requirements critique

First of all, it would be easier to understand the Requirements List if the order of the requirements was changed to match the game's flow (and maybe even split into different parts). Starting right away with the explanation of the round-based gameplay (increasing difficulty, 3 shots, 2 ducks), what actions the player could take (shoot with the gun) and what is the player's main objective (shoot the ducks down). Then proceeding to explain how the system responds to actions, like getting a hit (duck falls), miss all the ducks (dog laughs) and text display (nice shooting!). After that, more specific details (how many types of ducks).
I believe that technical specifications (white squares blinking to capture light data) wouldn't belong in a high level requirements list.

There are several incoherent specifications such as item 10. The time limit should be reduced, but a time limit was never specified in the first place. The closest to a time limit specification is item 8, but the wording makes it very unclear (as is could be a time limit, but couldn't; and if it was, why don't simply write 'time limit'?).
Item 9 specifies that the game should "reward good users" but a reward was never specified.
There is also no specification of the round and entities behavior (how does a round work? for how long are the ducks on the screen? do they enter the screen from either sides? is the round over only after the player spends their chances?).

In the Technical Architecture Diagram, it is specified that the gun would send either a trigger-pull OR an image capture. In reality the trigger pull will always be suceeded by an image capture, since the whole point of capturing the image is to check if the trigger was pressed in the moment the gun is pointing to one of the squares.
Some of the text is a bit vague, also: "The console sends an image to the television to be displayed" - wasn't it supposed to send a continuos stream of images to produce movement and thus enabling the game to be played? "The television displays an image that the light-gun sensor can capture" - it must also displays regular game frames so the user can see something besides white squares.


### 2. Short testing strategy
- Scope: the scope of the test will be to identify possible malfunctions in the system from the hardware parts (gun, console, tv) to the software (game).
- Approach: integration and UI tests will be performed with the individual system parts for functional and usability aspects, as follows:
	1. Functional testing of individual hardware parts
	2. Functional testing of gameplay and usability
	3. Automated UI testing of input controllers, data processing modules, rendering modules and game's logic
	4. Integration testing of aforementioned parts
	5. Bug tracking and assigning of detected malfunctions
	6. Re-testing
- Environment: the environment for functional tests should be the same setup as described in the Technical Architecture Diagram, using the latest software version and compatible OS. For UI and integration tests, there should be a supporting environment that allows for rapid input and output of data (such as image and controller input data) which should generate easy to document outputs.
- Exit criteria: the exit criteria will be in the shape of logs and bug reports, ensuring that all automated tests passed and no function or usability defect was found in functional testing.


### 3. Bug description

##### Bug: Possible system irresponsiveness to gun's captured image
- Description: player gun's trigger correctly fires the white boxes flash screen, but possibly fails to register entity hit - the player might be missing the targets.
- Reported by: customer service
- Severity: unknown
- Priority: high
- Steps to reproduce
	1. Boot up game
	2. Start a round
	3. Aim the gun to the a duck
	4. Press trigger
		- Expected: duck getting hit
		- Observed: duck doesn't get hit, dog laughs


### 4. Failing component identification
Since the failing component could be be anywhere between the gun's optical sensor (hardware level) and the system's captured image reading algorithm (software level), there's a wide scope to the test, so it would be necessary to test both levels.

The first testing approach will be to ask the customer support for more information about the external parts of the player's hardware (optical sensor, tv screen) to make sure that the light's intensity is not being physically reduced in any way. Then it would be necessary to perform a functional test as described by the player. If the hardware is setup accordingly and it was possible to replicate the bug, a test is required on the code responsible for turning the captured image data to make sure it is delivering the data as expected. If the problem is not there, the next inspection would be on the image reading algorithm and then the game logic's itself. It's important to notice that there's the possibility that there isn't a bug at all and the player is just bad at shooting ducks, so the tester must make sure that his/her aim is sharp.

The testing environment should replicate the player's hardware setup, in order to identify possible hardware-related failures - similar gun, console and TV models - and software versions. Automated testing could be used to generate and process images in order to verify full integrity of the image reading algorithm and following software tasks.
